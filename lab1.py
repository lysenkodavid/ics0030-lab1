#!/usr/bin/env python

import pandas
from matplotlib import pyplot

from common import describe_data
from common import test_env


def main():
    # Task 3 - Print out Python version and the available module versions.
    test_env.versions(['matplotlib', 'pandas'])

    # Task 4 - Read the dataset and print out the overview.
    df = pandas.read_csv('data/tasutud_maksud_2019_ii_kvartal_eng.csv',
                         sep=';', decimal=',', encoding='latin-1')
    print(describe_data.print_overview(df))

    # Task 5 - Print out all possible County column values and counts.
    print(df.groupby('County').size())

    # Task 6 - Print out all Type column unique values.
    print(df['Type'].unique())

    # Task 7 - New data frame with only the SME data.
    sme_df = df[df['Number of employees'] < 250]
    print(sme_df)

    # Task 8 - Extract "Number of employees" from previously created data frame.
    sme_employees = sme_df['Number of employees']

    # Task 9 - Calculate number of employees mean, median, mode, standard deviation and quartiles and print out the
    # results.
    sme_employees_mean = sme_employees.mean()
    sme_employees_median = sme_employees.median()
    sme_employees_mode = sme_employees.mode()
    sme_employees_std = sme_employees.std()
    sme_employees_quartiles = sme_employees.quantile([0.25, 0.5, 0.75])
    print("Mean, median, mode, std respectively: %d, %d, %d, %d" % (sme_employees_mean,
                                                                    sme_employees_median,
                                                                    sme_employees_mode,
                                                                    sme_employees_std))
    print("Quartiles:\n%s" % sme_employees_quartiles)

    # Task 10 - Employee histogram with mean, median and mode.
    figure1 = 'Employees Histogram'
    pyplot.figure(figure1)
    pyplot.hist(sme_employees, range=(sme_employees.min(),
                                      sme_employees.max()), bins=50, edgecolor='black')
    pyplot.title(figure1)
    pyplot.xlabel('Number of Employees')
    pyplot.ylabel('Count')
    pyplot.axvline(sme_employees_mean, color='r', linestyle='solid',
                   linewidth=1, label='Mean')
    pyplot.axvline(sme_employees_median, color='y', linestyle='dotted',
                   linewidth=1, label='Median')
    pyplot.axvline(sme_employees_mode[0], color='orange',
                   linestyle='dashed', linewidth=1, label='Mode')
    pyplot.legend()
    pyplot.savefig('results/figure1.png', papertype='a4')

    # Task 11 - Employees box plot.
    figure2 = 'Employees Box Plot'
    pyplot.figure(figure2)
    pyplot.boxplot(sme_employees)
    pyplot.ylabel('Number of Employees')
    pyplot.tick_params(axis='x', which='both',
                       bottom=False, top=False,
                       labelbottom=False)
    pyplot.title(figure2)
    pyplot.savefig('results/figure2.png', papertype='a4')
    pyplot.show()

    # Task 12 - Find correlation between Number of Employees and Labour Taxes and payments and print out correlation
    # matrix
    sme_df2 = sme_df[['Labour taxes and payments',
                      'Number of employees']].corr()
    print(sme_df2)

    # Task 13 - Scatter plot
    figure3 = 'Number of Employees, Labour Taxes and Payments correlation'
    pyplot.figure(figure3)
    pyplot.suptitle(figure3)
    pyplot.subplot(2, 2, 2)
    pyplot.scatter(sme_df2['Number of employees'],
                   sme_df2['Labour taxes and payments'],
                   color='red', s=0.5)
    pyplot.xlabel('Number of Employees')
    pyplot.ylabel('Labour Taxes and Payments')
    pyplot.subplot(2, 2, 3)
    pyplot.scatter(sme_df2['Labour taxes and payments'],
                   sme_df2['Number of employees'],
                   color='red', s=0.5)
    pyplot.ylabel('Number of Employees')
    pyplot.xlabel('Labour Taxes and Payments')
    pyplot.savefig('results/figure3.png', papertype='a4')
    pyplot.show()


if __name__ == '__main__':
    main()
